import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  a1 = 0;
  a2 = 0;
  result1 = 0;
  b1 = 0;
  b2 = 0;
  result2 = 0;
  c1 = 0;
  c2 = 0;
  result3 = 0;
  d1 = 0;
  d2 = 0;
  result4 = 0;

  constructor() {
    this.forLoop();
  }
  //* 簡單的計算機

  calculate() {
    this.result1 = this.a1 + this.a2;
  }
  calculate2() {
    this.result2 = this.b1 - this.b2;
  }
  calculate3() {
    this.result3 = this.c1 * this.c2;
  }
  calculate4() {
    this.result4 = this.d1 / this.d2;
  }

  //*  輸入一個數字，判斷此數字是否為質數。
  //todo 利用迴圈、判斷式、與 % 運算子處理。

  forLoop() {
    for (let i = 0; i < 100; i++) {
      if (this.findPrime(i)) {
        console.log(i + 1, '質數');
      } else {
        console.log(i + 1, '非質數');
      }
    }
  }

  findPrime(num: number) {
    if (num === 1) {
      return false;
    }
    for (let i = 2; i < num; i++) {
      if (num % i === 0) {
        return false;
      }
    }
    return true;
  }
}
